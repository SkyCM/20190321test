package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;

import java.io.*;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    public ComboBox<String> cbBlood;
    public ComboBox cbPlace;
    @FXML
    private GridPane mycomtroller;
    public DatePicker birthday;
    public RadioButton rbM;
    public ToggleGroup sexGroup;
    public RadioButton rbF;
    public RadioButton rbFree;
    public Button btnSelectAvatar;
    public ImageView myAvatar;

    public void doSelectBirthday(ActionEvent actionEvent) {
        System.out.println(birthday.getValue());
    }

    public void doSelectSex(ActionEvent actionEvent) {
        RadioButton radioButton = (RadioButton)actionEvent.getSource();
        System.out.println(radioButton.getText());
    }

    public void doSelectAvatar(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("影像檔","*.png","*.jpg","*.gif"));
        File selectedFile = fileChooser.showOpenDialog(mycomtroller.getScene().getWindow());
        System.out.println(selectedFile.getAbsolutePath());
        myAvatar.setImage(new Image(selectedFile.toURI().toString()));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<String> items = FXCollections.observableArrayList();
        items.addAll("O","A","B","AB");
        cbBlood.setItems(items);

        ObservableList<String> itemplaces = FXCollections.observableArrayList();

        String filename = "/Users/changshufu/Documents/程式語言/in.txt";
        String line;
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
            while ((line=bufferedReader.readLine())!=null){
                itemplaces.add(line);
            }
            bufferedReader.close();
            cbPlace.setItems(itemplaces);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
